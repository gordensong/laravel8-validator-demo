<?php

namespace App\Validators\Mysql;

class UsersAddressValidator extends \GordenSong\Laravel\Support\Validator
{
    use \App\Validators\Mysql\DatabaseRules\UsersAddressValidatorTrait;

    public function customizeRules(): array
    {
        return [
            'id' => ['required'],
            'user_id' => ['required'],
            'province' => ['required'],
            'city' => ['required'],
            'district' => ['required'],
            'created_at' => [],
            'updated_at' => [],
        ];
    }

    public function excludeRules(): array
    {
        return [
            'created_at',
            'updated_at',
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [
    // 'id' => '',
    // 'user_id' => '',
    // 'province' => '',
    // 'city' => '',
    // 'district' => '',
    // 'created_at' => '',
    // 'updated_at' => '',
    ];

    protected $scenes = [

    ];
}
