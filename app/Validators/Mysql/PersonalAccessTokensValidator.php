<?php

namespace App\Validators\Mysql;

class PersonalAccessTokensValidator extends \GordenSong\Laravel\Support\Validator
{
    use \App\Validators\Mysql\DatabaseRules\PersonalAccessTokensValidatorTrait;

    public function customizeRules(): array
    {
        return [
            'id' => ['required'],
            'tokenable_type' => ['required'],
            'tokenable_id' => ['required'],
            'name' => ['required'],
            'token' => ['required'],
            'abilities' => [],
            'last_used_at' => [],
            'created_at' => [],
            'updated_at' => [],
        ];
    }

    public function excludeRules(): array
    {
        return [
            'created_at',
            'updated_at',
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [
    // 'id' => '',
    // 'tokenable_type' => '',
    // 'tokenable_id' => '',
    // 'name' => '',
    // 'token' => '',
    // 'abilities' => '',
    // 'last_used_at' => '',
    // 'created_at' => '',
    // 'updated_at' => '',
    ];

    protected $scenes = [

    ];
}
