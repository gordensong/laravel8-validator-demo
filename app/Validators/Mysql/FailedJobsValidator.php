<?php

namespace App\Validators\Mysql;

class FailedJobsValidator extends \GordenSong\Laravel\Support\Validator
{
    use \App\Validators\Mysql\DatabaseRules\FailedJobsValidatorTrait;

    public function customizeRules(): array
    {
        return [
            'id' => ['required'],
            'uuid' => ['required'],
            'connection' => ['required'],
            'queue' => ['required'],
            'payload' => ['required'],
            'exception' => ['required'],
            'failed_at' => ['required'],
        ];
    }

    public function excludeRules(): array
    {
        return [
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [
    // 'id' => '',
    // 'uuid' => '',
    // 'connection' => '',
    // 'queue' => '',
    // 'payload' => '',
    // 'exception' => '',
    // 'failed_at' => '',
    ];

    protected $scenes = [

    ];
}
