<?php

namespace App\Validators\Mysql\DatabaseRules;

trait RuleValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'id' => [
                'integer',
                'min:0',
            ],
            'column' => [
                'regex:/^\d{1,2}:\d{1,2}:\d{1,2}(\.\d{1,6})?$/i',
            ],
        ];
    }
}
