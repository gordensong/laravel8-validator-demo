<?php

namespace App\Validators\Mysql\DatabaseRules;

trait PersonalAccessTokensValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'id' => [
                'integer',
                'min:0',
            ],
            'tokenable_type' => [
                'string',
                'max:255',
            ],
            'tokenable_id' => [
                'integer',
                'min:0',
            ],
            'name' => [
                'string',
                'max:255',
            ],
            'token' => [
                'string',
                'max:64',
            ],
            'abilities' => [
                'string',
                'max:65535',
            ],
            'last_used_at' => [
                'date',
            ],
            'created_at' => [
                'date',
            ],
            'updated_at' => [
                'date',
            ],
        ];
    }
}
