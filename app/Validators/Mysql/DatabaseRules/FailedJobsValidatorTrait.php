<?php

namespace App\Validators\Mysql\DatabaseRules;

trait FailedJobsValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'id' => [
                'integer',
                'min:0',
            ],
            'uuid' => [
                'string',
                'max:255',
            ],
            'connection' => [
                'string',
                'max:65535',
            ],
            'queue' => [
                'string',
                'max:65535',
            ],
            'payload' => [
                'string',
            ],
            'exception' => [
                'string',
            ],
            'failed_at' => [
                'date',
            ],
        ];
    }
}
