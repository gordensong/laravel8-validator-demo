<?php

namespace App\Validators\Mysql\DatabaseRules;

trait UsersAddressValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'id' => [
                'integer',
                'min:0',
            ],
            'user_id' => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'province' => [
                'string',
                'max:255',
            ],
            'city' => [
                'string',
                'max:255',
            ],
            'district' => [
                'string',
                'max:255',
            ],
            'created_at' => [
                'date',
            ],
            'updated_at' => [
                'date',
            ],
        ];
    }
}
