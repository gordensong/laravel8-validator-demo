<?php

namespace App\Validators\Mysql\DatabaseRules;

trait PasswordResetsValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'email' => [
                'string',
                'max:255',
            ],
            'token' => [
                'string',
                'max:255',
            ],
            'created_at' => [
                'date',
            ],
        ];
    }
}
