<?php

namespace App\Validators\Mysql\DatabaseRules;

trait UsersValidatorTrait
{
    public function tableRules(): array
    {
        return [
            'id' => [
                'integer',
                'min:0',
            ],
            'name' => [
                'string',
                'max:255',
            ],
            'email' => [
                'string',
                'max:255',
            ],
            'email_verified_at' => [
                'date',
            ],
            'password' => [
                'string',
                'max:255',
            ],
            'remember_token' => [
                'string',
                'max:100',
            ],
            'created_at' => [
                'date',
            ],
            'updated_at' => [
                'date',
            ],
        ];
    }
}
