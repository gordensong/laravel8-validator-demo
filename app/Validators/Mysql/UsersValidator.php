<?php

namespace App\Validators\Mysql;

class UsersValidator extends \GordenSong\Laravel\Support\Validator
{
	use \App\Validators\Mysql\DatabaseRules\UsersValidatorTrait;

	public function customizeRules(): array
	{
		return [
			'id' => ['required'],
			'name' => ['required', 'min:3', 'max:50'],
			'password' => ['required'],
			'created_at' => [],
			'updated_at' => [],
		];
	}

	public function excludeRules(): array
	{
		return [
			'created_at',
			'updated_at',
		];
	}

	protected $messages = [

	];

	protected $attributes = [
		// 'id' => '',
		// 'name' => '',
		// 'password' => '',
		// 'created_at' => '',
		// 'updated_at' => '',
	];

	protected $scenes = [
		'name-register' => ['name', 'password' => 'confirmed'],
	];
}
