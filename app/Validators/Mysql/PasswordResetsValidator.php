<?php

namespace App\Validators\Mysql;

class PasswordResetsValidator extends \GordenSong\Laravel\Support\Validator
{
    use \App\Validators\Mysql\DatabaseRules\PasswordResetsValidatorTrait;

    public function customizeRules(): array
    {
        return [
            'email' => ['required'],
            'token' => ['required'],
            'created_at' => [],
        ];
    }

    public function excludeRules(): array
    {
        return [
            'created_at',
        ];
    }

    protected $messages = [

    ];

    protected $attributes = [
    // 'email' => '',
    // 'token' => '',
    // 'created_at' => '',
    ];

    protected $scenes = [

    ];
}
