<?php

namespace App\Http\Controllers;

use App\Validators\Mysql\UsersAddressValidator;
use App\Validators\Mysql\UsersValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function nameRegister(Request $request, UsersValidator $validator): JsonResponse
    {
        $validated = $validator->scene('name-register')->validate($request->all());

        return response()->json($validated);
    }

    public function addAddresses(Request $request, UsersAddressValidator $addressesValidator)
    {
        $validated = $addressesValidator
            ->only('province', 'city', 'district')
            ->prefix('addresses.*')
            ->validate($request->all());

        return response()->json($validated);
    }

}
