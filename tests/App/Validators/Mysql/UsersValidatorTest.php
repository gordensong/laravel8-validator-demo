<?php

namespace App\Validators\Mysql;

use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class UsersValidatorTest extends TestCase
{
	public function test_register_with_name_password()
	{
		$validator = UsersValidator::instance()
			->addRule('password', ['confirmed'])
			->only('name', 'password');

		self::assertEquals([
			'name' => ['required', 'string', 'max:50', 'min:3',],
			'password' => ['required', 'string', 'max:255', 'confirmed',],
		], $validator->rules());

		$data = [
			'name' => 'jack',
			'password' => '123456',
			'password_confirmation' => '123456',
		];

		$validated = Validator::make(
			$data,
			$validator->rules(),
			$validator->messages(),
			$validator->attributes()
		)->validated();

		self::assertEquals([
			'name' => 'jack',
			'password' => '123456',
		], $validated);
	}

	private function registerData(): array
	{
		return [
			'name' => 'jack',
			'password' => '123456',
			'password_confirmation' => '123456',
		];
	}

	public function test_scene()
	{
		$registerData = $this->registerData();

		$usersValidator = UsersValidator::instance()->scene('name-register');

		$validated = $usersValidator->validate($registerData);

		self::assertEquals([
			'name' => 'jack',
			'password' => '123456',
		], $validated);

		self::assertEquals([
			'name' => ['required', 'string', 'max:50', 'min:3',],
			'password' => ['required', 'string', 'max:255', 'confirmed',],
		], $usersValidator->rules());
	}
}
