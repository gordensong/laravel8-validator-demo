<?php

namespace Tests\App\Http\Controllers;

use Tests\TestCase;

class UserControllerTest extends TestCase
{
    public function test_nameRegister($route = 'user.name-register')
    {
        $data = [
            'name' => 'jack',
            'password' => '123456',
            'password_confirmation' => '123456',
        ];

        $this->postJson(route($route), $data)
            ->assertOk()
            ->assertJson([
                'name' => 'jack',
                'password' => '123456',
            ]);
    }

    public function test_addAddresses($route = 'user.add-addresses')
    {
        $data = [
            'addresses' => [
                ['province' => 'PA1', 'city' => 'CA1', 'district' => 'DA1'],
                ['province' => 'PA2', 'city' => 'CA2', 'district' => 'DA2'],
            ],
        ];

        $this->postJson(route($route), $data)
            ->assertOk()
            ->assertJson($data);
    }
}
